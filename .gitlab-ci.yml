# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

stages:
  - lint
  - prepare
  - test
  - branch
  - deploy
  - complete

workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_COMMIT_BRANCH =~ /^release\/v\d+.\d+$/'
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_MERGE_REQUEST_IID'
    - if: '$CI_PIPELINE_SOURCE == "web"'


# Common parameters
# ===========================================================================
default:
  tags:
    - docker


# Shared jobs
# ===========================================================================
.git:
  image: "registry.hub.docker.com/gitscm/git:latest"
  variables:
    GIT_DEPTH: '0'
  before_script:
    - git --version
    - git config --local user.name "${GITLAB_USER_NAME}"
    - git config --local user.email "${GITLAB_USER_EMAIL}"
    - git config --local credential.helper "cache --timeout=2147483647"
    - |
      printf "url=%s\nusername=%s\npassword=%s\n\n" \
             "${CI_PROJECT_URL}" \
             "${CI_BOT_USER}" \
             "${CI_PERSONAL_TOKEN}" | \
      git credential approve
    - git remote set-url --push origin "https://${CI_BOT_USER}@${CI_REPOSITORY_URL#*@}"
  after_script:
    - git credential-cache exit


.docker:
  image: "registry.hub.docker.com/library/docker:stable"
  before_script:
    - docker --version
    - |
      echo "${CI_JOB_TOKEN}" | \
      docker login \
                   --password-stdin \
                   --username gitlab-ci-token \
                   "${CI_REGISTRY}"


# Linting
# ===========================================================================
.linting:
  stage: lint

shellscript_linting:
  extends: .linting
  image: "registry.hub.docker.com/koalaman/shellcheck-alpine:stable"
  before_script:
    - shellcheck --version
  script:
    - |
      find "." \
           -not -path "./.git/*" \
           -type f \( -path "./githooks/*" -o -iname "*.sh" \) \
           -exec shellcheck \
                            --color=always \
                            --external-sources \
                            --format=tty \
                            --shell=sh \
                            "{}" +

dockerfile_linting:
  extends: .linting
  image: "registry.hub.docker.com/hadolint/hadolint:latest-debian"
  before_script:
    - hadolint --version
  script:
    - |
      find "." \
           -not -path "./.git/*" \
           -type f -iname "*ockerfile*" \
           -exec hadolint --format tty "{}" +


# Build environment setup
# ===========================================================================
prepare_build_environment:
  extends: .docker
  stage: prepare
  script:
    - |
      branch_version="$(echo "${CI_COMMIT_BRANCH:-}" | \
                        sed -n 's|^release\/\(v[[:digit:]]\+\.[[:digit:]]\+\)$|\1|p')"
      branch_version="${branch_version:-$(echo "${CI_COMMIT_TAG}" | \
                                          sed -n 's|^\(v[[:digit:]]\+\.[[:digit:]]\+\).*$|\1|p')}"
      if [ "${CI_COMMIT_TAG%%-rc1}" = "${CI_COMMIT_TAG}" ] && \
         [ -n "${branch_version:-}" ] && \
         docker pull "${CI_REGISTRY_IMAGE}:${branch_version}"; then
        echo "Re-using existing container '${CI_REGISTRY_IMAGE}:${branch_version:=Unknown}'"
        docker tag \
               "${CI_REGISTRY_IMAGE}:${branch_version}" \
               "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
      else
        echo "Building container '${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}'"
        docker build \
               --pull \
               --rm \
               --tag "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" \
               "./"
      fi
    - |
      docker run \
             --rm \
             "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" \
             "/test/buildenv_check.sh"


# Testing
# ===========================================================================
.test:
  image: "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  stage: test
  script: "test/${CI_JOB_NAME}.sh"

build:
  extends: .test

sign_image:
  extends: .test


# Deploy containers
# ===========================================================================
deploy:
  extends: .docker
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+(\.\d+){2,}(-rc\d+)?$/'
  retry:
    max: '2'
    when:
      - script_failure
  script:
    - docker tag "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/raw:${CI_COMMIT_SHORT_SHA}"
    - docker push "${CI_REGISTRY_IMAGE}/raw:${CI_COMMIT_SHORT_SHA}"
    - docker tag "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
    - |
      if [ "${CI_COMMIT_TAG%%-rc1}" != "${CI_COMMIT_TAG}" ]; then
        docker tag "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG%.*}"
        docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG%.*}"
      fi
    - |
      latest_major_minor="$(wget -q -O - \
                                 --header "PRIVATE-TOKEN: ${CI_PERSONAL_TOKEN}" \
                                 "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories?tags=true" | \
                            tr '}' '\n' | \
                            sed -n 's|.*"tags":.*"path":"'${CI_PROJECT_PATH}':v\([[:digit:]]\+\.[[:digit:]]\+\).*|\1|p' | \
                            sort -n -r -t '.' | \
                            head -n 1)"
    - |
      if [ -z "${latest_major_minor:-}" ]; then
        echo "First registry tag '${CI_COMMIT_TAG}'"
        latest_major_minor="0.0"
      fi
    - latest_major="${latest_major_minor%%.*}"
    - docker_tag_major="${CI_COMMIT_TAG%%.*}"
    - |
      if [ "${docker_tag_major#v*}" -ge "${latest_major:?}" ]; then
        latest_minor="${latest_major_minor#*.}"
        docker_tag_minor_patch="${CI_COMMIT_TAG#${docker_tag_major:?}.}"
        docker_tag_minor="${docker_tag_minor_patch%.*}"
        docker_tag_patch="${docker_tag_minor_patch#*.}"
        if [ "${docker_tag_major#v*}" -ne "${latest_major}" ] || \
           [ "${docker_tag_minor}" -ge "${latest_minor:?}" ]; then
          echo "Pushing '${CI_COMMIT_TAG}' as latest"
          docker tag  "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}:latest"
          docker push "${CI_REGISTRY_IMAGE}:latest"
        fi
      fi


# Release branch
# ===========================================================================
create_release_branch:
  extends: .git
  stage: branch
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+$/'
  script:
    - |  # Workaround for gitlab-org/gitlab#237792
      CI_COMMIT_TAG_MESSAGE="$(git tag \
                                       --format="%(contents:subject)%0a%0a%(contents:body)" \
                                       --list ${CI_COMMIT_TAG:?} | sed 's|\r |\n|g')"
    - |
      if [ -z "${CI_COMMIT_TAG_MESSAGE:-}" ]; then
        CI_COMMIT_TAG_MESSAGE="$(printf "See tag '%s'\n\n(Auto-created release candidate)" "${CI_COMMIT_TAG:?}")"
      fi
    - git checkout "${CI_COMMIT_SHA}" -b "release/${CI_COMMIT_TAG:?}"
    - git tag --annotate --message="${CI_COMMIT_TAG_MESSAGE}" "${CI_COMMIT_TAG}.0-rc1"
    - git push --follow-tags origin "HEAD"


# Complete
# ===========================================================================
announce_release:
  image: "registry.gitlab.com/gitlab-org/release-cli:latest"
  stage: complete
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+(\.\d+)+$/'
      when: delayed
      start_in: 86 minutes
  interruptible: false
  script:
    - |  # Workaround for gitlab-org/gitlab#237792
      apk add --no-cache git && \
      CI_COMMIT_TAG_MESSAGE="$(git tag \
                                       --format="%(contents:subject)%0a%0a%(contents:body)" \
                                       --list ${CI_COMMIT_TAG:?} | sed 's|\r |\n|g')"
    - |
      if [ -z "${CI_COMMIT_TAG_MESSAGE:-}" ]; then
        CI_COMMIT_TAG_MESSAGE="$(printf "See tag '%s'\n\n(Auto-created release candidate)" "${CI_COMMIT_TAG:?}")"
      fi
    - |  # Workaround for gitlab-org/gitlab#237893
      release-cli create \
                         --description "${CI_COMMIT_TAG_MESSAGE:-No release notes.}" \
                         --name "${CI_PROJECT_TITLE} ${CI_COMMIT_TAG}" \
                         --tag-name "${CI_COMMIT_TAG}"

cleanup_docker_container:
  extends: .docker
  stage: complete
  when: always
  variables:
    GIT_STRATEGY: none
    GIT_SUBMODULE_STRATEGY: none
  script:
    - |
      if docker inspect --type image "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" 1> "/dev/null"; then
        docker rmi "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
      fi
