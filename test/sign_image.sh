#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2021 EVBox Intelligence B.V.
# Copyright (C) 2021 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"

METADATA_CHAIN_SIZE_BYTES=4
METADATA_CHAIN_SIZE_MAX="$((128 * 1024))"
METADATA_SIGNATURE_SIZE_BYTES=4
METADATA_SIGNATURE_SIZE_MAX=1024

set +eu


generate_key()
{
	_key_output="${1:?Missing argument to function}"
	_cert_output="${2:?Missing argument to function}"
	_key_pub_output="${3:?Missing argument to function}"

	openssl req -x509 \
	            -nodes \
	            -newkey "rsa:1024" \
	            -keyout "${_key_output}" \
	            -out "${_cert_output}" \
	            -days 365 \
	            -subj '/CN=localhost' > "/dev/null" 2>&1

	openssl rsa -in "${_key_output}" -pubout -out "${_key_pub_output}"
}

extract_metadata()
{
	_update_file="${1:?Missing argument to function}"
	_update_signature="${2:?Missing argument to function}"


	_signature_size="$((0x$(tail -c "${METADATA_SIGNATURE_SIZE_BYTES}" "${_update_file}" | \
	                       od -An -t x4 | \
	                       tr -d " \t")))"
	if [ "${_signature_size}" -gt "${METADATA_SIGNATURE_SIZE_MAX}" ] || \
	   [ "${_signature_size}" -le 0 ]; then
		fail "Unexpected signature size '${_signature_size}'"
		return
	fi
	metadata_size="$((_signature_size + METADATA_SIGNATURE_SIZE_BYTES))"

	tail -c "${metadata_size}" "${_update_file}" | \
	head -c "${_signature_size}" > "${_update_signature}"
}

verify_update()
{
	_update_file="${1:?Missing argument to function}"
	_pub_key="${2:?Missing argument to function}"
	_update_signature="${3:?Missing argument to function}"

	if ! head -c "-${metadata_size}" "${_update_file}" | \
	   openssl dgst \
	           -sha512 \
	           --keyform "PEM" \
	           -verify "${_pub_key}" \
	           -signature "${_update_signature}" \
	           "-"; then
		fail "Invalid or corrupt software update file"
		return
	fi
}


verify_signature()
{
	_update_file="${1:?Missing argument to function}"
	_pub_key="${2:?Missing argument to function}"
	extract_metadata "${_update_file}" "${update_signature}"
	verify_update "${_update_file}" "${_pub_key}" "${update_signature}"
}


extract_chain()
{
	_update_file="${1:?Missing argument to function}"
	_certificate_chain="${2:?Missing argument to function}"

	signature_size="$((0x$(tail -c "${METADATA_SIGNATURE_SIZE_BYTES}" "${_update_file}" | \
	                       od -An -t x4 | \
	                       tr -d " \t")))"
	if [ "${signature_size}" -gt "${METADATA_SIGNATURE_SIZE_MAX}" ] || \
	   [ "${signature_size}" -le 0 ]; then
		fail "Unexpected signature size '${signature_size}'"
	fi
	_metadata_size="$((signature_size + METADATA_SIGNATURE_SIZE_BYTES))"

	_chain_size="$((0x$(head -c "-${_metadata_size}" "${_update_file}" | \
	                   tail -c "${METADATA_CHAIN_SIZE_BYTES}" | \
	                   od -An -t x4 | \
	                   tr -d " \t")))"

	if [ "${_chain_size}" -gt "${METADATA_CHAIN_SIZE_MAX}" ] || \
	   [ "${_chain_size}" -le 0 ]; then
		fail "Unexpected chain size '${_chain_size}'"
	fi

	_metadata_size="$((_metadata_size + METADATA_CHAIN_SIZE_BYTES))"
	_metadata_size="$((_metadata_size + _chain_size))"

	tail -c "${_metadata_size}" "${_update_file}" | \
	head -c "${_chain_size}" > "${_certificate_chain}"
}


verify_cert()
{
	_update_file="${1:?Missing argument to function}"
	_cert="${2:?Missing argument to function}"
	extract_chain "${_update_file}" "${test_signature_output}/chain_extracted"
	if ! cmp "${test_signature_output}/chain_extracted" "${_cert}"; then
		fail "Chain wrongly stored"
	fi
}


setUp()
{
	test_signature_output="$(mktemp -d -p "${SHUNIT2_TMPDIR:-/tmp}" "test_signature_output.XXXXXX")"
	mkdir -p "${test_signature_output}"
	generate_key "${test_signature_output}/key.pem" "${test_signature_output}/cert.crt" "${test_signature_output}/key.pub"

	update_path="${test_signature_output}/update"
	update_signature="${test_signature_output}/update.sig"

	cp "${FIXTURES}/vmlinux" "${update_path}"

	echo
}

tearDown()
{
	if [ -d "${test_signature_output}" ]; then
		rm -f -r "${test_signature_output:?}"
	fi
	echo "--------------------------------------------------------------------------------"
}

# CONDITIONS
# - Matching Key/Certificate
# - No options
# EXPECTED OUTCOME
# - a signed image is created
# - the certificates are correctly included
# - the file is signed correctly
testSimpleSign()
{

	"${COMMAND_UNDER_TEST}" \
	                        -c "${test_signature_output}/cert.crt" \
	                        -k "${test_signature_output}/key.pem" \
	                        "${update_path}"

	assertTrue "Simple update should be signed" "[ ${?} -eq 0 ]"

	verify_cert "${update_path}.sign" \
	            "${test_signature_output}/cert.crt"

	verify_signature "${update_path}.sign" \
	                 "${test_signature_output}/key.pub"

}

# CONDITIONS
# - Mismatching key and certificate
# - No options
# EXPECTED OUTCOME
# - an error code is returned
# - no file is created
testWrongKeyCert()
{

	generate_key "${test_signature_output}/key2.pem" \
	             "${test_signature_output}/cert2.crt" \
	             "${test_signature_output}/key2.pub"

	"${COMMAND_UNDER_TEST}" \
	                        -c "${test_signature_output}/cert.crt" \
	                        -k "${test_signature_output}/key2.pem" \
	                        "${update_path}"

	assertTrue "Wrong cert/key combination should fail" "[ ${?} -ne 0 ]"
	assertTrue "There is no file leftover" "[ ! -f ${update_path}.sign ]"
}

# CONDITIONS
# - Matching Key/Certificate
# - No options / With overwrite flag
# - A file is already existing in output path
# EXPECTED OUTCOME
# - Without -f option, the call fail
# - With -f option, the update is signed
testForceOverwrite()
{
	echo "hello" > "${update_path}.sign"

	"${COMMAND_UNDER_TEST}" \
	                        -c "${test_signature_output}/cert.crt" \
	                        -k "${test_signature_output}/key.pem" \
	                        "${update_path}"

	assertTrue "Update should fail if a file already exist" "[ ${?} -ne 0 ]"
	if [ "hello" != "$(cat "${update_path}.sign")" ]; then
		fail "File shouldn't be overwritten without the force flag"
	fi

	"${COMMAND_UNDER_TEST}" \
	                        -c "${test_signature_output}/cert.crt" \
	                        -f \
	                        -k "${test_signature_output}/key.pem" \
	                        "${update_path}"

	assertTrue "Update should be signed" "[ ${?} -eq 0 ]"

	verify_cert "${update_path}.sign" \
	            "${test_signature_output}/cert.crt"

	verify_signature "${update_path}.sign" \
	                 "${test_signature_output}/key.pub"
}

# CONDITIONS
# - Matching Key/Certificate
# - Specify output file
# EXPECTED OUTCOME
# - file created at destination
testCustomOutputFile()
{
	test_custom_output="$(mktemp -p "${SHUNIT2_TMPDIR:-/tmp}" -u "test_custom_output.XXXXXX")"
	"${COMMAND_UNDER_TEST}" \
	                        -c "${test_signature_output}/cert.crt" \
	                        -k "${test_signature_output}/key.pem" \
	                        -o "${test_custom_output}" \
	                        "${update_path}"

	assertTrue "Update should be signed"  "[ ${?} -eq 0 ]"

	verify_cert "${test_custom_output}" \
	            "${test_signature_output}/cert.crt"

	verify_signature "${test_custom_output}" \
	                 "${test_signature_output}/key.pub"

	assertTrue "There is a custom output file" "[ -f ${test_custom_output} ]"
	assertTrue "The default location is empty" "[ ! -f ${update_path}.sign ]"
}
