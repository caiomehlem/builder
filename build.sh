#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_ARCHIVE_COMPRESSION="xz"
DEF_FIT_IMAGE_TEMPLATE="templates/fitImage.its.in"
DEF_FIT_SCRIPT_TEMPLATE="templates/fit_script.its.in"
DEF_KEEP_BUILD_CACHE="false"
DEF_OUTPUT_DIR="output"
DEF_VERIFY_HASH_ALGO="sha256"

# Extended attributes are explicitly disabled to avoid (future) errors
# Also we have no way of controlling them at build time right now anyway
SQUASHFS_ARGS="-no-xattrs"

TEMPLATE_INC_CONFIGURATIONS="templates/fitConfigurations.its.inc.in"
TEMPLATE_INC_DEVICETREE="templates/fitDevicetree.its.inc.in"
TEMPLATE_INC_FPGA="templates/fitFPGA.its.inc.in"
TEMPLATE_INC_INITRAMFS="templates/fitInitramfs.its.inc.in"
TEMPLATE_INC_SIGNATURE="templates/fitSignature.its.inc.in"


usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo "This script generates a bootable FIT image to be used with u-boot."
	echo "    -a  Set the target architecture, an empty var performs a native build (current: '${TARGET_ARCH:-native}'). [TARGET_ARCH]"
	echo "    -b  Build config directory to use [BUILD_CONFIG]"
	echo "    -c  Use previous build cache (default: ${DEF_KEEP_BUILD_CACHE}) [KEEP_BUILD_CACHE]"
	echo "    -d  Supply a device tree blob, directory or name [ESBS_DEVICETREE]"
	echo "    -f  Supply a FPGA bitstream file or name [ESBS_FPGA_BITSTREAM]"
	echo "    -h  Print this usage"
	echo "    -i  Supply an initramfs image for early boot [ESBS_INIT_RAMDISK]"
	echo "    -k  Supply a kernel image or name [ESBS_KERNEL]"
	echo "    -o  Set the output directory (default: '${DEF_OUTPUT_DIR}/\${ARCH}/\${BUILD_CONFIG}') [OUTPUT_DIR]"
	echo "    -p  Comma separated list of additional packages to install [PACKAGE_LIST]"
	echo "    -r  Semi-colon or comma separated list or file, with single line entries, of extra repositories to use [REPOSITORIES]"
	echo "    -s  Secure boot key (pair) (<filename> [and <filename>.pub]) or (concatenated priv and pub) key PEM format [SECURE_BOOT_KEY]"
	echo "    -t  Use custom fit template (default: '${DEF_FIT_IMAGE_TEMPLATE}') [FIT_IMAGE_TEMPLATE]"
	echo "    -u  Allow untrusted packages to be installed. WARNING: This prevents signature validation. [UNTRUSTED]"
	echo "    -v  Verification hashing algorithm to use (default '${DEF_VERIFY_HASH_ALGO}') [VERIFY_HASH_ALGO]"
	echo "    -x  Archive compression type (default: '${DEF_ARCHIVE_COMPRESSION}') [ARCHIVE_COMPRESSION]"
	echo
	echo "All options can also be passed in environment variables (listed between [BRACKETS])."
	echo "NOTE: This script requires administrative privileges to run."
}

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg "err" "${*}" >&2
}

e_warn()
{
	_msg "warning" "${*}"
}

e_notice()
{
	_msg "notice" "${*}"
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	src_file="$(readlink -f "${0}")"
	src_dir="${src_file%%${src_file##*/}}"

	case "${arch}" in
	"aarch64" | "arm64v8")
		alt_arch="arm64"
		;;
	"amd64")
		alt_arch="x86_64"
		;;
	"arm"*)
		alt_arch="arm"
		;;
	*)
		alt_arch="${arch}"
		;;
	esac

	case "${archive_compression:-none}" in
	"bzip2")
		archive_compressor="$(command -v bzip2) -9 -f"
		archive_ext="bz2"
		;;
	"cpio" | "none")
		archive_compressor="cat"
		;;
	"gzip")
		archive_compressor="$(command -v gzip) -n -9 -f"
		archive_ext="gz"
		;;
	"lz4")
		archive_compressor="$(command -v lz4) -l -9 -f"
		archive_ext="lz4"
		;;
	"lzma")
		archive_compressor="$(command -v lzma) -9 -f"
		archive_ext="lzma"
		;;
	"lzo")
		archive_compressor="$(command -v lzop) -9 -f"
		archive_ext="lzo"
		;;
	"xz")
		archive_compressor="$(command -v xz) -9 --check=crc32 --lzma2=dict=1MiB"
		archive_ext="xz"
		;;
	"zstd")
		archive_compressor="$(command -v zstd) --ultra -22"
		archive_ext="zstd"
		;;
	*)
		e_err "Unsupported compression scheme '${archive_compression}'."
		e_err "Use bzip2, cpio, gzip, lz4, lzma, lzo, xz or zstd."
		exit 1
		;;
	esac

	if [ "${keep_build_cache}" != "true" ] && \
	   [ -d "${build_dir}" ]; then
		rm -f -r "${build_dir:?}"
	fi
	mkdir -p "${build_dir}"

	os_version="unknown"
	if ! _os_version_hash="g$(git rev-parse --short HEAD 2> "/dev/null")"; then
		_os_version_hash="unknown"
	else
		if ! os_version="$(git describe --always --dirty 2> "/dev/null")"; then
			os_version="${_os_version_hash}"
		fi
	fi
	build_id="$(date -Iseconds -u)-${_os_version_hash}"

	if [ -n "${secure_boot_key}" ]; then
		if [ ! -f "${secure_boot_key}" ]; then
			_secure_boot_key="$(mktemp -p "${TMPDIR:-/tmp}" "secure_boot_key_rsa-XXXXXXXX")"
			touch "${_secure_boot_key}.pub"
			remind_key_cleanup="${remind_key_cleanup:-} '${_secure_boot_key}' '${_secure_boot_key}.pub'"

			printf "%s" "${secure_boot_key%%-----END PRIVATE KEY-----*}-----END PRIVATE KEY-----" | \
			fold > "${_secure_boot_key}"

			printf "%s" "${secure_boot_key#*-----END PRIVATE KEY-----}" | \
			sed '/./,$!d' | \
			fold > "${_secure_boot_key}.pub"

			secure_boot_key="${_secure_boot_key}"
		fi

		if [ ! -f "${secure_boot_key:-}.pub" ]; then
			e_notice "No valid key '${secure_boot_key:-}.pub', generating new public key ..."
			openssl pkey \
				-in "${secure_boot_key}" \
				-out "${secure_boot_key}.pub" \
				-outform "PEM" \
				-pubout

			e_notice "Storing copy of generated public key from private key '${secure_boot_key}' in '${build_dir}'"
			cp "${secure_boot_key}.pub" "${build_dir}"

			remind_key_cleanup="${remind_key_cleanup:-} '${secure_boot_key}.pub'"
		fi

		crypto_cipher="$(openssl rsa \
		                         -in "${secure_boot_key}" \
		                         -text \
		                         -noout | \
		                 sed -n 's|.*Private-Key:[[:space:]]\+(\([[:digit:]]\+\)[[:space:]]bit.*|rsa\1|p')"
	fi

	if [ -e "${devicetree:-}" ]; then
		if [ -L "${devicetree}" ]; then
			devicetree="$(readlink -f "${devicetree}")"
		fi
		if [ "${devicetree#/*}" = "${devicetree}" ]; then
			devicetree="$(pwd)/${devicetree}"
		fi
	fi

	if [ -e "${fpga_bitstream:-}" ]; then
		if [ -L "${fpga_bitstream}" ]; then
			fpga_bitstream="$(readlink -f "${fpga_bitstream}")"
		fi
		if [ "${fpga_bitstream#/*}" = "${fpga_bitstream}" ]; then
			fpga_bitstream="$(pwd)/${fpga_bitstream}"
		fi
	fi

	if [ -f "${init_ramdisk:-}" ]; then
		if [ -L "${init_ramdisk}" ]; then
			init_ramdisk="$(readlink -f "${init_ramdisk}")"
		fi
		if [ "${init_ramdisk#/*}" = "${init_ramdisk}" ]; then
			init_ramdisk="$(pwd)/${init_ramdisk}"
		fi
	fi

	if [ -f "${kernel:-}" ]; then
		if [ -L "${kernel}" ]; then
			kernel="$(readlink -f "${kernel}")"
		fi
		if  [ "${kernel#/*}" = "${kernel}" ]; then
			kernel="$(pwd)/${kernel}"
		fi
	fi

	if [ -n "${fit_image_template:-}" ]; then
		if [ ! -f "${fit_image_template}" ]; then
			e_err "Supplied template is not a file"
			exit 1
		fi

		if [ -L "${fit_image_template}" ]; then
			fit_image_template="$(readlink -f "${fit_image_template}")"
		fi
		if [ "${fit_image_template#/*}" = "${fit_image_template}" ]; then
			fit_image_template="$(pwd)/${fit_image_template}"
		fi
	fi

	if [ -L "${build_config}" ]; then
		build_config="$(readlink -f "${build_config}")"
	fi

	if [ "${build_config#/*}" = "${build_config}" ]; then
		build_config="$(pwd)/${build_config}"
	fi
	if [ ! -d "${build_config}" ]; then
		e_warn "Missing build configuration '${build_config}'."
		printf "Create new one? (y/n): "
		read -r __sure
		echo
		if [ "${__sure}" != "Y" ] && \
		   [ "${__sure}" != "y" ]; then
			e_err "Cannot continue without configuration, aborting."
			exit 1
		fi

		mkdir -p "${build_config}"
	fi

	if [ -z "${output_dir:-}" ]; then
		e_err "Output directory undefined, cannot continue"
		exit 1
	fi
	if [ -L "${output_dir}" ]; then
		output_dir="$(readlink -f "${output_dir}")"
	fi
	if [ "${output_dir##/*}" = "${output_dir}" ]; then
		output_dir="$(pwd)/${output_dir}"
	fi
	if [ -d "${output_dir}" ]; then
		rm -f -r "${output_dir:?}"
	fi
	mkdir -p "${output_dir}"
}

cleanup()
{
	if [ "${keep_build_cache}" != "true" ] && \
	   [ -d "${build_dir:-}" ]; then
		rm -f -r "${build_dir:?}"
	fi

	if [ -n "${remind_key_cleanup:-}" ]; then
		e_notice "Generated or installed keys, remember to clean or store them:"
		e_notice "${remind_key_cleanup}" | tr -s ' ' '\n'
		e_notice "-----------------------------------------------------------------"
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

## make_squashfs_archive() - Create squashfs archive
# @input_dir:   Directory that needs to be archived
# @output_file: Filename of the resulting image archive
#
# Removes any existing @output_file and generates a new squashfs archive
# compressed by @archive_compressor as @output_file.
make_squashfs_archive()
{
	_input_dir="${1:?Missing argument to function}"
	_output_file="${2:?Missing argument to function}"

	if [ -f "${_output_file}" ]; then
	    unlink "${_output_file}"
	fi
	mksquashfs "${_input_dir}" "${_output_file}" \
		   ${archive_compression:+-comp ${archive_compression}} \
	           ${SQUASHFS_ARGS:+${SQUASHFS_ARGS}}
}

## make_cpio_archive() - Create cpio archive
# @input_dir:   Directory that needs to be archived
# @output_file: Filename of the resulting image archive
#
# Removes any existing @output_file and generates a new compressed cpio archive
# of @input_dir. Archive compression is determined by @archive_compressor.
make_cpio_archive()
{
	_input_dir="${1:?Missing argument to function}"
	_output_file="${2:?Missing argument to function}"

	if [ -f "${_output_file}" ]; then
	    unlink "${_output_file}"
	fi
	(
		cd "${_input_dir}"

		find "./" | \
		cpio -o -H newc | \
		${archive_compressor:+${archive_compressor}} \
	) > "${_output_file}"
}

## make_archive() - Create either a cpio or squashfs archive
# @image_dst_dir:       Directory that needs to be archived
#
# Wrapper function around @make_cpio_archive or @make_squashfs_archive,
# determining the archive type by the directory name which must end in either
# `.cpio.root` to indicate a CPIO archive or `.squahsfs.root` to indicate
# a squashfs archive.
#
# Prints (returns) the destination archive name as a string on success,
# returns 1 on failure
make_archive()
{
	_image_dst_dir="${1:?Missing argument to function}"

	unset _archive_name
	if [ "${_image_dst_dir%%.cpio.root}" != "${_image_dst_dir}" ]; then
		_archive_name="${build_dir}/$(basename "${_image_dst_dir%.cpio.root}").cpio${archive_ext:+.${archive_ext}}"
		make_cpio_archive "${_image_dst_dir}" "${_archive_name}" 1>&2
	fi

	if [ "${_image_dst_dir%%.squashfs.root}" != "${_image_dst_dir}" ]; then
		_archive_name="${build_dir}/$(basename "${_image_dst_dir%.squashfs.root}")${archive_ext:+.${archive_ext}}.squashfs"
		make_squashfs_archive "${_image_dst_dir}" "${_archive_name}" 1>&2
	fi

	if [ -z "${_archive_name:-}" ]; then
		return 1
	fi

	echo "${_archive_name}"
}

## bootstrap_alpine_rootfs() - Bootstrap Alpine linux into @bootstrap_dir
# @bootstrap_dir:       Output location to bootstrap Alpine into
# @bootstrap_package_list:      File containing a list of Alpine packages
#
# Bootstrap Alpine Linux into @bootstrap_dir using `bootstrap_alpine_rootfs.sh`
bootstrap_alpine_rootfs()
{
	_bootstrap_dir="${1:?Missing argument to function}"
	_bootstrap_package_list="${2:?Missing argument to function}"
	_bootstrap_config_base="${build_config}"

	# Setting repositories in the environment variable will store them
	# in the image as /etc/apk/repositories
	_alpine_repositories="$(basename "${_bootstrap_package_list%*.apklist}.repositories")"
	if [ -f "${_alpine_repositories}" ]; then
		ALPINE_REPOS="$(sed 's|#.*||g' "${_alpine_repositories}")"
	fi

	for _repository in $(echo "${repositories}" | \
	                     tr -s ' ,;\f\n\t\v' ' '); do
		if [ -z "${_repository}" ]; then
			continue
		fi

		if [ -f "${_repository}" ]; then
			__repositories="${__repositories:-} $(sed 's|#.*||g' "${_repository}")"
		else
			__repositories="${__repositories:-} ${_repository}"
		fi
	done
	for _repository in $(echo "${__repositories:-}" | \
	                     tr -s ' ,;\f\n\t\v' ' '); do
		if [ -z "${_repository}" ]; then
			continue
		fi

		if [ -e "${_repository}" ]; then
			_repository="$(readlink -f "${_repository}")"
		fi

		_repositories="${_repositories:-} ${_repository}"
	done
	_repositories="$(echo "${_repositories:-}" | \
	                 sed -e 's|^[[:space:]]||g' | \
	                 tr -s ' ,;\f\n\t\v' ',')"
	for _repository in "${src_dir}/packages/"*; do
		if [ ! -d "${_repository}" ]; then
			continue
		fi

		_repositories="${_repositories:-},$(readlink -f "${_repository}")"
	done

	if [ -f "${_bootstrap_package_list}" ]; then
		package_list="${package_list};$(sed 's|#.*||g' "${_bootstrap_package_list:-}")"
	fi

	if [ -n "${package_list}" ]; then
		_packages="-p $(echo "${package_list}" | \
		                tr -s ' ;\f\n\t\v' ';')"
	fi

	for _pub_key in "${_bootstrap_config_base}/"*".pub"; do
		_pub_keys="-k ${_pub_key} ${_pub_keys:-}"
	done

	(
		ALPINE_REPOS="${ALPINE_REPOS:-}" \
		"${src_dir}/bootstrap_alpine_rootfs.sh" \
		                                        -a "${arch}" \
		                                        -b "${_bootstrap_dir}/" \
		                                        ${untrusted:+-u} \
		                                        ${_pub_keys:+${_pub_keys}} \
		                                        ${_packages:+${_packages}} \
		                                        ${_repositories:+-r ${append_repositories:+,}${_repositories:-}}
	)
	if [ ! -f "${_bootstrap_dir}/etc/alpine-release" ]; then
		e_err "Alpine Linux bootstrap failed for '${_bootstrap_package_list}'."
		exit 1
	fi

	id_like="alpine"
}

## bootstrap_rootfs() - Bootstrap an OS into a directory
# @bootstrap_dir:       Output directory to bootstrap into
# @bootstrap_package_list:      File containing package entries
#
# Take a list of packages and bootstrap those into @bootstrap_dir. The package
# filename extension determines which OS is to be bootstrap.
#
# Currently supported extensions are:
# - *.apklist:  Bootstrap Alpine Linux
bootstrap_rootfs()
{
	_bootstrap_dir="${1:?Missing argument to function}"
	_bootstrap_package_list="${2:?Missing argument to function}"

	if [ "${_bootstrap_package_list%%.apklist}" != "${_bootstrap_package_list}" ]; then
		bootstrap_alpine_rootfs "${_bootstrap_dir}" "${_bootstrap_package_list}"
	fi

	if [ -d "${_bootstrap_package_list%.*list}.files/" ]; then
		cp -T -a "${_bootstrap_package_list%.*list}.files/" \
		   "${_bootstrap_dir}"
	fi

	if [ ! -f "${_bootstrap_dir}/init" ]; then
		ln -f -s "/sbin/init" "${_bootstrap_dir}/init"
	fi

	if [ -f "${_bootstrap_package_list%.*list}.os-release" ]; then
		_os_release="${_bootstrap_package_list%.*list}.os-release"
	elif [ -f "${build_config}/os-release" ]; then
		_os_release="${build_config}/os-release"
	else
		_os_release="${src_dir}/templates/os-release"
	fi
	mkdir -p "${_bootstrap_dir}/usr/lib/"
	sed \
	    -e "s|@OS_VERSION@|${os_version}|g" \
	    -e "s|@ID_LIKE@|${id_like:=}|g" \
	    -e "s|@BUILD_ID@|${build_id}|g" \
	    "${_os_release}" > "${_bootstrap_dir}/usr/lib/os-release"
	ln -f -s "../usr/lib/os-release" "${_bootstrap_dir}/etc/os-release"
}

## create_squashfs_image() - Create a squashfs formatted image archive
# @pkglist:         File containing a list of packages
# @output_file:     Output filename of the archive
#
# Takes a list of packages from a file and bootstraps these into a squashfs
# formatted image archive. It is copmressed as per @archive_compressor.
create_squashfs_image()
{
	_pkglist="${1:?Missing argument to function}"
	_output_file="${2:?Missing argument to function}"

	if [ ! -f "${_pkglist}" ]; then
		e_warn "File not found: '${_pkglist}'"
		return
	fi

	_image_name="$(basename "${_pkglist%%.squashfs.*list}")"
	bootstrap_rootfs "${build_dir}/${_image_name}" "${_pkglist}"
	make_squashfs_archive "${build_dir}/${_image_name}" "${_output_file}"
}

## create_cpio_image() - Create an cpio formatted image archive
# @pkglist:         File containing a list of packages
# @output_file:     Output filename of the archive
#
# Takes a list of packages from a file and bootstraps these into a cpio
# formatted image archive. It is compressed as per @archive_compressor.
create_cpio_image()
{
	_pkglist="${1:?Missing argument to function}"
	_output_file="${2:?Missing argument to function}"

	if [ ! -f "${_pkglist}" ]; then
	    e_warn "File not found: '${_pkglist}'"
	    return
	fi

	_image_name="$(basename "${_pkglist%%.cpio.*list}")"
	bootstrap_rootfs "${build_dir}/${_image_name}" "${_pkglist}"
	make_cpio_archive "${build_dir}/${_image_name}" "${_output_file}"
}

## create_fit_image() - Create a FIT image
# @fit_image_src:       File in `.its` format to U-Boot's mkimage
# @fit_image_dst:       Output filename for the final FIT image
#
# Create a FIT image, transforming the input @fit_image_src file by replacing
# known markers with content, and use mkimage to generate a FIT image. The
# image is finally signed with the secure boot key if the `configuration` node
# is present.
create_fit_image()
{
	_fit_image_src="${1:?Missing argument to function}"
	_fit_image_dst="${2:?Missing argument to function}"
	_fit_mkimage_its="${build_dir}/$(basename "${_fit_image_dst}").mkimage.its"

	if [ ! -f "${_fit_image_src}" ]; then
		e_warn "File not found: '${_fit_image_src}'"
		return
	fi

	if [ -n "${crypto_cipher:-}" ]; then
		sed \
		    -e "s|@CRYPTO_CIPHER@|${crypto_cipher:-}|g" \
		    -e "s|@KEY_NAME_HINT@|$(basename "${secure_boot_key%%.*}")|g" \
		    -e 's|@UBOOT_IGNORE@|uboot-ignore = <1>;|g' \
		    "${_fit_image_src}" > "${_fit_mkimage_its}"
	else
		sed \
		    -e '/@CRYPTO_CIPHER@/d' \
		    -e '/@KEY_NAME_HINT@/d' \
		    -e '/@UBOOT_IGNORE@/d' \
		    "${_fit_image_src}" > "${_fit_mkimage_its}"
	fi

	sed -i \
	    -e "s|@ARCH@|${alt_arch}|g" \
	    -e "s|@BOARD_NAME@|${build_name}|g" \
	    -e "s|@BUILD_DIR@|${build_dir}|g" \
	    -e "s|@BUILD_ID@|${build_id}|g" \
	    -e "s|@COMPRESSION_TYPE@|${archive_compression:-none}|g" \
	    -e "s|@VERIFY_HASH_ALGO@|${verify_hash_algo}|g" \
	    -e "s|@VERSION@|${os_version}|g" \
	    "${_fit_mkimage_its}"

	# mkimage can only sign FIT images if they contain a 'configurations' node
	_config_node="$(grep "configurations" "${_fit_mkimage_its}" || true)"

	mkdir -p "$(dirname "${_fit_image_dst}")"
	mkimage -V
	mkimage -f "${_fit_mkimage_its}" \
	        ${_config_node:+${secure_boot_key:+-K ${secure_boot_key}.pub -k ${secure_boot_key}}} \
	        "${_fit_image_dst}"
}

## generate_fit_template() - Generate a fit template file
# @fit_image_src:       Fit image source template file
# @fit_iamge_dst:       Fit image destination template file
# @fit_iamge_dtbs:      Directory containing devicetree files
# @fit_image_fgpa_bits: File, Directory or name containing fpga bitstream(s)
#
# Generate a fit template by populating known fields. This to allow template
# for the most common tasks to be generated, including multiple devicetree's
# and/or (multiple) FPGA bitstreams.
#
# All target files for the template will be compress using @archive_compressor
# into @build_dir.
generate_fit_template()
{
	_fit_image_src="${1:?Missing argument to function}"
	_fit_image_dst="${2:?Missing argument to function}"
	_fit_image_dtbs="${3:?Missing argument to function}"
	_fit_image_fpga_bits="${4:-}"

	if [ ! -f "${_fit_image_dtbs}" ]; then
		e_err "Not a file, '${_fit_image_dtbs}'"
		exit 1
	fi

	_fit_image_fpga_dirs="${build_config}/"
	if [ -d "${_fit_image_fgpa_bits:-}" ]; then
		_fit_image_fpga_dirs="${_fit_image_fpga_dirs} ${_fit_image_fpga_bits}"
	fi
	if [ -f "${_fit_image_fpga_bits:-}" ]; then
		_fit_image_fpga_dirs="$(dirname "${_fit_image_fpga_bits}")"
		_fit_fpga_name="$(basename "${_fit_image_fpga_bits%.bi?}")"
	fi
	if [ ! -e "${_fit_image_fpga_bits:-}" ]; then
		_fit_fpga_name="${_fit_image_fpga_bits}"
	fi

	_fit_image_dtb="$(basename "${_fit_image_dtbs%.dtb*}")"
	_fit_image_dtbs="$(dirname "${_fit_image_dtbs}")"

	cp "${_fit_image_src}" "${_fit_image_dst}"

	find "${_fit_image_dtbs}" \
	     -follow \
	     -maxdepth 1 \
	     -type f \
	     -iname '*.dtb' | while read -r _dtb; do
		_dtb_filename="$(basename "${_dtb}")"
		_dtb_name="${_dtb_filename%.dtb*}"
		_fit_dtb="${build_dir}/${_dtb_filename}${archive_ext:+.${archive_ext}}"

		eval "${archive_compressor}" < "${_dtb}" > "${_fit_dtb}"

		unset _fit_initramfs
		for _initramfs in "${build_dir}/initramfs."*; do
			_initramfs_name="${_initramfs#*/initramfs.}"
			if [ "${_initramfs_name#${_dtb_name}.cpio*}" != "${_initramfs_name}" ] || \
			   [ "${_initramfs_name#${_dtb_name}.squashfs*}" != "${_initramfs_name}" ]; then
				_fit_initramfs="${_initramfs}"
				break;
			fi
		done
		if [ -z "${_fit_initramfs:-}" ]; then
			_fit_initramfs_multiconf="s|@INITRAMFS_MULTI_CONF@|@INITRAMFS_CONF@|g"
		else
			_fit_initramfs_multiconf="s|@INITRAMFS_MULTI_CONF@|ramdisk = \"initramfs-${_dtb_name}\";|g"
		fi

		unset _fit_fpga
		for _fpga_bit_dir in ${_fit_image_fpga_bits}; do
			if [ -z "${_fit_fpga_name:-}" ]; then
				_fit_fpga_name="${_dtb_name}"
			fi
			for _fpga_bit_file in "${_fpga_bit_dir}/${_fit_fpga_name}.bi"?; do
				if [ ! -e "${_fpga_bit_file}" ]; then
					continue
				fi
				_fit_fpga="${build_dir}/${_fit_fpga_name}.bit${archive_ext:+.${archive_ext}}"

				eval "${archive_compressor}" < "${_fpga_bit_file}" > "${_fit_fpga}"
				break
			done
		done
		if [ -z "${_fit_fpga:-}" ]; then
			_fit_fpga_multiconf="s|@FPGA_BITSTREAM_MULTI_CONF@|@FPGA_BITSTREAM_CONF@|g"
		else
			_fit_fpga_multiconf="s|@FPGA_BITSTREAM_MULTI_CONF@|fpga = \"fpga-${_fit_fpga_name}\";|g"
		fi

		sed -i \
		    -e "/@CONFIGURATIONS_INC@/{hgr ${src_dir}/${TEMPLATE_INC_CONFIGURATIONS}" -e 'N}' \
		    ${_fit_fpga:+-e "/@FPGA_BITSTREAM_INC@/{hgr ${src_dir}/${TEMPLATE_INC_FPGA}" -e 'N}'} \
		    -e "/@DEVICETREE_INC@/{hgr ${src_dir}/${TEMPLATE_INC_DEVICETREE}" -e 'N}' \
		    ${_fit_initramfs:+-e "/@INITRAMFS_INC@/{hgr ${src_dir}/${TEMPLATE_INC_INITRAMFS}" -e 'N}'} \
		    "${_fit_image_dst}"
		sed -i \
		    ${crypto_cipher:+-e "/@SIGNATURE_CONF@/{r ${src_dir}/${TEMPLATE_INC_SIGNATURE}" -e 'd}'} \
		    -e "s|@DEVICETREE_NAME@|${_dtb_name}|g" \
		    -e "${_fit_fpga_multiconf}" \
		    ${_fit_fpga:+-e "s|@FPGA_BITSTREAM_NAME@|${_fit_fpga_name}|g"} \
		    -e "s|@DATA_DEVICETREE@|${_fit_dtb}|g" \
		    ${_fit_initramfs:+-e "s|@DATA_INITRAMFS@|${_fit_initramfs}|g"} \
		    -e "${_fit_initramfs_multiconf}" \
		    ${_fit_initramfs:+-e "s|@INITRAMFS_NAME@|${_dtb_name}|g"} \
		    ${_fit_fpga:+-e "s|@DATA_FPGA_BITSTREAM@|${_fit_fpga}|g"} \
		    "${_fit_image_dst}"
	done

	sed -i \
	    -e "s|@DEFAULT_CONFIG@|default = \"conf-$(basename "${_fit_image_dtb}")\";|g" \
	    -e '/@CONFIGURATIONS_INC@/d' \
	    -e '/@DEVICETREE_INC@/d' \
	    -e '/@SIGNATURE_CONF@/d' \
	    "${_fit_image_dst}"
}

## create_boot_fit_image() - Create a bootable FIT image
# @its_image_src:       File in `.its` format to U-Boot's mkimage
# @its_image_dst:       Output filename for the final FIT image in @build_dir
# @fit_kernel:          Kernel image to include in FIT image
# @fit_initramfs:       Optional ramdisk to be used for the image
#
# Create a FIT image, transforming the input @its_image_src file by replacing
# known markers with content and call @create_fit_iamge() to generate a FIT
# image.
# The supplied @fit_kernel will be compressed using @archive_compressor if set
# via @archive_ext.
# If a devicetree was supplied to the script as a file, this will be equally
# compressed and added to the FIT image.
# If a FGPA bitstream was supplied to the script as a file, this will be equally
# compressed and added to the FIT image.
#
# The resulting fitImage is installed only if the extension of the output
# filename ends in `.itb`. Any other extensions are treated as internal
# components and will stay only in @build_dir.
create_boot_fit_image()
{
	_its_image_src="${1:?Missing argument to function}"
	_its_image_dst="${2:?Missing argument to function}"
	_fit_kernel="${3:?Missing argument to function}"
	_fit_initramfs="${4:-}"

	_its_image_tmp="${build_dir}/$(basename "${_its_image_dst}.tmp.its.in")"

	_fit_kernel_image="${build_dir}/$(basename "${_fit_kernel%%.${archive_ext:-}}")${archive_ext:+.${archive_ext}}"
	if [ -f "${_fit_kernel}" ] && \
	   [ "${_fit_kernel}" != "${_fit_kernel_image}" ]; then
		eval "${archive_compressor}" < "${_fit_kernel}" > "${_fit_kernel_image}"
	fi

	_fit_fdt_image="${build_dir}/$(basename "${devicetree%%.${archive_ext:-}}")${archive_ext:+.${archive_ext}}"
	if [ -f "${devicetree}" ] && \
	   [ "${devicetree}" != "${_fit_fdt_image}" ]; then
		eval "${archive_compressor}" < "${devicetree}" > "${_fit_fdt_image}"
	fi

	_fit_fpga_image="${build_dir}/$(basename "${fpga_bitstream%%.${archive_ext:-}}")${archive_ext:+.${archive_ext}}"
	if [ -f "${fpga_bitstream}" ] && \
	   [ "${fpga_bitstream}" != "${_fit_fpga_image}" ]; then
		eval "${archive_compressor}" < "${fpga_bitstream}" > "${_fit_fpga_image}"
	fi

	if [ -f "${_fit_initramfs}" ]; then
		sed \
		    -e 's|@INITRAMFS_\(MULTI_\)\?CONF@|ramdisk = "initramfs-1";|g' \
		    -e "/@INITRAMFS_INC@/{r ${src_dir}/${TEMPLATE_INC_INITRAMFS}" -e 'd}' \
		    -e 's|@INITRAMFS_SIGN@|, "ramdisk"|g' \
		    "${_its_image_src}" > "${_its_image_tmp}"
	else
		sed \
		    -e '/@INITRAMFS_\(MULTI_\)\?CONF@/d' \
		    -e '/@INITRAMFS_INC@/d' \
		    -e '/@INITRAMFS_SIGN@/d' \
		    "${_its_image_src}" > "${_its_image_tmp}"
	fi
	if [ -f "${_fit_fpga_image:-}" ]; then
		sed -i \
		    -e 's|@FPGA_BITSTREAM_\(MULTI_\)\?CONF@|fpga = "fpga-1";|g' \
		    -e "/@FPGA_BITSTREAM_INC@/{r ${src_dir}/${TEMPLATE_INC_FPGA}" -e 'd}' \
		    -e 's|@FPGA_BITSTREAM_SIGN@|, "ramdisk"|g' \
		    "${_its_image_tmp}"
	else
		sed -i \
		    -e '/@FPGA_BITSTREAM_\(MULTI_\)\?CONF@/d' \
		    -e '/@FPGA_BITSTREAM_INC@/d' \
		    -e '/@FPGA_BITSTREAM_SIGN@/d' \
		    "${_its_image_tmp}"
	fi
	sed -i \
	    -e "s|@DATA_DEVICETREE@|${_fit_fdt_image:-}|g" \
	    -e "s|@DATA_FPGA_BITSTREAM@|${_fit_fpga_image:-}|g" \
	    -e "s|@DATA_INITRAMFS@|${_fit_initramfs:-}|g" \
	    -e "s|@DATA_KERNEL@|${_fit_kernel_image:-}|g" \
	    -e "s|@FPGA_BITSTREAM_NAME@|1|g" \
	    -e "s|@INITRAMFS_NAME@|1|g" \
	    -e "s|@KERNEL_NAME@| '$(basename "${_fit_kernel_image:-}")'|g" \
	    "${_its_image_tmp}"

	create_fit_image "${_its_image_tmp}" "${build_dir}/${_its_image_dst}"
	if [ "${_its_image_dst%.itb}" != "${_its_image_dst}" ]; then
		cp "${build_dir}/${_its_image_dst}" "${output_dir}/"
	fi
}

## build_script_fit_image() - Generate a U-Boot script fit image
#
# Scans @build_config for filenames ending in `.scr` and either a matching
# `.scr.its.in` its file or the script template is then used to generate a
# U-Boot script fit image ending in `.scr.uimg`.
build_script_fit_image()
{
	find "${build_config}" \
	     -maxdepth 1 \
	     -type f \
	     -iname "*.scr" | while read -r _fit_script; do
		_script_name="$(basename "${_fit_script%%.scr}")"
		if [ -f "${_fit_script}.its.in" ]; then
			_script_its="${_fit_script}.its.in"
		fi

		sed \
		    -e "s|@DATA_SCRIPT@|${_fit_script}|g" \
		    "${_script_its:=${src_dir}/${DEF_FIT_SCRIPT_TEMPLATE}}" > "${build_dir}/${_script_name}.scr.its.in"

		create_fit_image "${build_dir}/${_script_name}.scr.its.in" \
		                 "${build_dir}/${_script_name}.scr.uimg"

		cp "${build_dir}/${_script_name}.scr.uimg" "${output_dir}"
	done
}

## build_cpio_image() - Generate a cpio based filesystem image archive
#
# Scans @build_config for filenames ending in `.cpio.*list` that do not start
# with `rootfs` and passes that on to @create_cpio_image. Only files that are
# not named `initramfs` are installed into @output_dir. Files that are named
# `initramfs` are considered for internal purposes.
#
# If the file created is in fact `initramfs.*` and a initramfs is also supplied
# via the `-i` parameter, then this is considered a critical error.
build_cpio_image()
{
	find "${build_config}" \
	     -maxdepth 1 \
	     -type f \
	     -not -iname 'fitImage*' \
	     -not -iname 'rootfs*' \
	     -iname "*.cpio.*list" | while read -r _pkglist; do
		_output_file="${build_dir}/$(basename "${_pkglist%%.cpio.*list}").cpio${archive_ext:+.${archive_ext}}"

		create_cpio_image "${_pkglist}" "${_output_file}"

		if [ "${_pkglist##*initramfs}" = "${_pkglist}" ]; then
			cp "${_output_file}" "${output_dir}"
		fi
	done
	if [ -f "${build_dir}/initramfs.cpio${archive_ext:+.${archive_ext}}" ]; then
		if [ -n "${init_ramdisk}" ]; then
			e_err "Cannot continue, ESBS_INIT_RAMDISK and/or multiple initramfs.cpio.*list both set."
			exit 1
		fi
		init_ramdisk="${build_dir}/initramfs.cpio${archive_ext:+.${archive_ext}}"
	fi
}

## build_squashfs_image() - Generate a squashfs based filesystem image archive
#
# Scans @build_config for filenames ending in `.squashfs.*.list` that do not
# start with `rootfs` and passes that on to @create_squashfs_image. Only files
# that are not named `initramfs` are installed into @output_dir. Files that are
# named `initramfs` are considered for ingternal purposes.
#
# If the file created is in fact `initramfs.*` and initramfs is also supplied
# via the `-i` parameter, then this is considered a critical error.
build_squashfs_image()
{
	find "${build_config}" \
	     -maxdepth 1 \
	     -type f \
	     -not -iname 'fitImage*' \
	     -not -iname 'rootfs*' \
	     -iname "*.squashfs.*list" | while read -r _pkglist; do
		_output_file="${build_dir}/$(basename "${_pkglist%.squashfs.*list}")${archive_ext:+.${archive_ext}}.squashfs"

		create_squashfs_image "${_pkglist}" "${_output_file}"

		if [ "${_pkglist##*initramfs}" = "${_pkglist}" ]; then
			cp "${_output_file}" "${output_dir}"
		fi
	done
	if [ -f "${build_dir}/initramfs${archive_ext:+.${archive_ext}}.squashfs" ]; then
		if [ -n "${init_ramdisk}" ]; then
			e_err "Cannot continue, ESBS_INIT_RAMDISK and/or multiple initramfs.squashfs.*list both set."
			exit 1
		fi
		init_ramdisk="${build_dir}/initramfs${archive_ext:+.${archive_ext}}.squashfs"
	fi
}

## build_simple_fit_image() - Create a U-Boot bootable FIT image
# Requires a valid kernel to be set via the `-k`, a valid devicetree via `-d`
# and a valid initramfs via `-i` script parameters.
#
# Uses the supplied @fit_image_template to generate a U-Boot bootable fitimage
# using @create_boot_fit_image. If no template is supplied, `.its` or `.its.in`
# files in @build_config are searched and used as templates instead.
build_simple_fit_image()
{
	if [ ! -f "${kernel:-}" ]; then
		return
	fi

	if [ -z "${fit_image_template:-}" ]; then
		_fit_image_configs="$(find "${build_config}" \
		                           -maxdepth 1 \
		                           -type f \
		                           -iname "*.its" -o \
		                           -iname "*.its.in")"
		if [ -f "${devicetree:-}" ] && \
		   [ -n "${_fit_image_configs}" ]; then
			echo "${_fit_image_configs}" | while read -r _fit_image_config; do
				create_boot_fit_image "${_fit_image_config}" \
				                      "${_fit_image_config%.its*}.itb" \
				                      "${kernel}" \
				                      "${init_ramdisk}"
			done
			_fit_image_configs="$(echo "${_fit_image_configs}" |\
			                      wc -l)"
		fi
	fi

	if [ -f "${fit_image_template:-}" ] || \
	   [ "${_fit_image_configs:-0}" -le 0 ]; then
		if [ ! -f "${init_ramdisk}" ]; then
			e_err "Missing file '${init_ramdisk}' for parameter ESBS_INIT_RAMDISK."
			usage
			exit 1
		fi

		if [ ! -f "${fit_image_template:-}" ]; then
			fit_image_template="${build_dir}/$(basename "${DEF_FIT_IMAGE_TEMPLATE%%.in}.in")"
			generate_fit_template "${src_dir}/${DEF_FIT_IMAGE_TEMPLATE}" \
			                      "${fit_image_template}" \
			                      "${devicetree:-}"
		fi
		create_boot_fit_image "${fit_image_template}" \
		                      "${fit_image_template%.its*}.itb" \
		                      "${kernel}" \
		                      "${init_ramdisk}"
	fi
}

## bootstrap_with_kernel() - Bootstrap into a directory and extract the kernel
#
# @image_pkglist:     pkglist to bootstrap files with
# @image_dst_dir:     directory name to put bootstrap content into
# @image_fit_template_dst: Destination file from the template generator
#
# Bootstrap a rootfs and move the kernel out of the @image_dst_dir, compress
# it with the @archive_compressor. Likewise, the devicetrees and FPGA bitstream
# are compressed, but these are copied and not removed from the target
# directory into the $build_dir.
#
# The intermediate rootfs is generated in @image_dst_dir from @image_pglist.
# The generated template is stored in @image_fit_template_dst.
# Prints the final compressed kernel file name upon success.
#
bootstrap_with_kernel()
{
	_image_pkglist="${1:?Missing argument to function}"
	_image_dst_dir="${2:?Missing argument to function}"
	_image_fit_template_dst="${3:?Missing argument to function}"

	_image_fit_template_config="${build_config}/$(basename "${_image_pkglist%.*list}")"
	if [ -f "${_image_fit_template_config}.its.in" ]; then
		_image_fit_template_src="${_image_fit_template_config}.its.in"
	elif [ -f "${_image_fit_template_config}.its" ]; then
		_image_fit_template_src="${_image_fit_template_config}.its"
	elif [ -f "${fit_image_template}" ]; then
		_image_fit_template_src="${fit_image_template}"
	else
		_image_fit_template_src="${src_dir}/${DEF_FIT_IMAGE_TEMPLATE%%.in}.in"
	fi

	bootstrap_rootfs "${_image_dst_dir}" "${_image_pkglist}" 1>&2

	if [ -z "${devicetree:-}" ]; then
		_rootfs_dtbs="${_image_dst_dir}/boot/dtbs/"
	elif [ ! -e "${devicetree}" ]; then
		_rootfs_dtbs="${_image_dst_dir}/boot/dtbs/${devicetree}"
		if [ "${_rootfs_dtbs%.dtb}" = "${_rootfs_dtbs}" ]; then
			_rootfs_dtbs="${_rootfs_dtbs}.dtb"
		fi
	elif [ -f "${devicetree}" ]; then
		_rootfs_dtbs="${devicetree}"
	else
		e_err "No devicetree supplied or found."
		exit 1
	fi

	if [ -d "${_rootfs_dtbs}" ]; then
		_rootfs_dtbs="$(find "${_rootfs_dtbs}" \
		                     -type f \
		                     -iname "*.dtb" | \
		                head -n 1)"
	fi

	if [ -z "${fpga_bitstream:-}" ]; then
		_fpga_bitstream="${_image_dst_dir}/lib/firmware/fpga"
	else
		_fpga_bitstream="${fpga_bitstream}"
	fi

	generate_fit_template "${_image_fit_template_src}" \
	                      "${_image_fit_template_dst}" \
	                      "${_rootfs_dtbs:-}" \
	                      "${_fpga_bitstream}"

	if [ -n "${kernel:-}" ]; then
		if [ -f "${kernel:-}" ]; then
			_rootfs_kernel="${kernel}"
		else
			_rootfs_kernel="${_image_dst_dir}/boot/${kernel}"
		fi
	else
		for _kernel_image in "${_image_dst_dir}/boot/vmlinu"*; do
			if [ ! -L "${_kernel_image}" ]; then
				continue
			fi
			_rootfs_kernel="$(readlink -f "${_kernel_image}")"
			break
		done
	fi
	if [ ! -f "${_rootfs_kernel:-}" ]; then
		e_err "Missing kernel '${_rootfs_kernel:-}' needed for building boot fit image."
		exit 1
	fi

	_rootfs_kernel_comp="${build_dir}/$(basename "${_rootfs_kernel}")${archive_ext:+.${archive_ext}}"
	eval "${archive_compressor}" < "${_rootfs_kernel}" > "${_rootfs_kernel_comp}"
	echo "Moved into fitImage '${_image_dst_file}'" > "${_rootfs_kernel}"

	echo "${_rootfs_kernel_comp}"
}

## build_fitImage() - Built a fitImage
#
# Scans @build_config for filenames starting with `fitImage`
# and passes that on to @bootstrap_with_kernel.
build_fitImage()
{
	find "${build_config}" \
	     -maxdepth 1 \
	     -type f \
	     -iname "fitImage*.*list" \
	     -not -iname '*~' | while read -r _pkglist; do
		__image_dst="$(basename "${_pkglist%.*list}")"
		_image_dst_dir="${build_dir}/${__image_dst}.root"
		_fit_image_template_dst="${build_dir}/${__image_dst}.its.in"
		_image_dst_file="${__image_dst}${archive_ext:+.${archive_ext}}.itb"

		_rootfs_kernel="$(bootstrap_with_kernel "${_pkglist}" \
		                                        "${_image_dst_dir}" \
		                                        "${_fit_image_template_dst}")"

		if ! _image_archive="$(make_archive "${_image_dst_dir}")"; then
			continue
		fi

		create_boot_fit_image "${_fit_image_template_dst}" \
				      "${_image_dst_file}" \
				      "${_rootfs_kernel}" \
				      "${_image_archive}"
	done
}

## build_rootfs_image() - Build a rootfs filesystem image
#
# Scans @build_config for filenames starting with `rootfs`
# and passes that on to @bootstrap_rootfs.
#
# Following this the *fitImage template* (either supplied or the default
# template) is then used to generate a fitImage to inject into the rootfs
# in @build_dir.
build_rootfs_image()
{
	find "${build_config}" \
	     -maxdepth 1 \
	     -type f \
	     -iname "rootfs*.*list" \
	     -not -iname '*~' | while read -r _pkglist; do
		__image_dst="$(basename "${_pkglist%.*list}")"
		_image_dst_dir="${build_dir}/${__image_dst}.root"
		_fit_image_template_dst="${build_dir}/${__image_dst}.its.in"
		_image_dst_file="fitImage"

		_rootfs_kernel="$(bootstrap_with_kernel "${_pkglist}" \
		                                        "${_image_dst_dir}" \
		                                        "${_fit_image_template_dst}")"

		create_boot_fit_image "${_fit_image_template_dst}" \
		                      "${_image_dst_file}" \
		                      "${_rootfs_kernel}" \
		                      "${init_ramdisk}"

		if [ ! -d "${_image_dst_dir}/boot/" ]; then
			mkdir -p "${_image_dst_dir}/boot/"
		fi
		cp "${build_dir}/fitImage" "${_image_dst_dir}/boot/"

		if ! _image_archive="$(make_archive "${_image_dst_dir}")"; then
			continue
		fi
		cp "${_image_archive}" "${output_dir}/$(basename "${_image_archive#rootfs.}")"
	done
}

main()
{
	_start_time="$(date "+%s")"

	while getopts ":a:b:cd:f:hi:k:o:p:r:s:t:uv:x:" _options; do
		case "${_options}" in
		a)
			arch="${OPTARG}"
			;;
		b)
			build_config="${OPTARG}"
			;;
		c)
			keep_build_cache="true"
			;;
		d)
			devicetree="${OPTARG}"
			;;
		f)
			fpga_bitstream="${OPTARG}"
			;;
		h)
			usage
			exit 0
			;;
		i)
			init_ramdisk="${OPTARG}"
			;;
		k)
			kernel="${OPTARG}"
			;;
		o)
			output_dir="${OPTARG}"
			;;
		p)
			package_list="${OPTARG};${package_list:-}"
			;;
		r)
			repositories="${OPTARG},${repositories:-}"
			if [ "${repositories#,}" != "${repositories}" ]; then
				append_repositories="true"
			fi
			;;
		s)
			secure_boot_key="${OPTARG}"
			;;
		t)
			fit_image_template="${OPTARG}"
			;;
		u)
			untrusted="true"
			e_warn "No signatures will be checked, this is for development purposes only!"
			;;
		v)
			verify_hash_algo="${OPTARG}"
			;;
		x)
			archive_compression="${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ -z "${build_config:=${BUILD_CONFIG:-}}" ]; then
		for _config in "configs/"*; do
			if [ -d "${_config}" ]; then
				build_config="${_config}"
				break
			fi
		done
	fi
	if [ -z "${build_config}" ]; then
		e_err "Missing build configuration directory, try using '-b'."
		usage
		exit 1
	fi

	arch="${arch:-${TARGET_ARCH:-$(uname -m)}}"
	build_name="$(basename "${build_config%%/}")"
	build_dir="${build_dir:-${BUILD_DIR:-$(pwd)/.build/${arch}/${build_name}}}"
	devicetree="${devicetree:-${ESBS_DEVICETREE:-}}"
	fit_image_template="${fit_image_template:-${FIT_IMAGE_TEMPLATE:-}}"
	fpga_bitstream="${fpga_bitstream:-${ESBS_FPGA_BITSTREAM:-}}"
	init_ramdisk="${init_ramdisk:-${ESBS_INIT_RAMDISK:-}}"
	keep_build_cache="${keep_build_cache:-${KEEP_BUILD_CACHE:-${DEF_KEEP_BUILD_CACHE}}}"
	kernel="${kernel:-${ESBS_KERNEL:-}}"
	output_dir="${output_dir:-${OUTPUT_DIR:-$(pwd)/${DEF_OUTPUT_DIR}/${arch}/${build_name}}}"
	package_list="${package_list:-${PACKAGE_LIST:-}}"
	repositories="${repositories:-${REPOSITORIES:-}}"
	secure_boot_key="${secure_boot_key:-${SECURE_BOOT_KEY:-}}"
	untrusted="${untrusted:-${UNTRUSTED:-}}"
	verify_hash_algo="${verify_hash_algo:-${VERIFY_HASH_ALGO:-${DEF_VERIFY_HASH_ALGO}}}"

	init

	build_cpio_image
	build_squashfs_image
	build_script_fit_image
	build_simple_fit_image
	build_fitImage
	build_rootfs_image

	echo "==============================================================================="
	echo "Build report for $(date -u)"
	echo "Version: ${os_version:-error}"
	echo "Build ID: ${build_id:-error}"
	echo
	for _output_file in "${output_dir}/"*; do
		if [ ! -f "${_output_file:-}" ]; then
			continue
		fi

		printf "$(basename "${_output_file}"): %7s\n" \
		       "$(du -d 0 -h "$(readlink -f "${_output_file}")" | \
			  cut -f 1)"
	done
	echo
	echo "Successfully built image(s) for '${build_name}' in $(($(date "+%s") - _start_time)) seconds"
	echo "==============================================================================="

	cleanup
}

main "${@}"

exit 0
