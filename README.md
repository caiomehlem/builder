# Builder
This repository contains scripts to generate a few different kinds of images.

* bootable [FIT](https://git.denx.de/?p=u-boot.git;a=blob;f=doc/uImage.FIT/howto.txt) image to be used with [U-Boot](https://www.denx.de/wiki/U-Boot/)
* bootstrap [alpine linux](https://www.alpinelinux.org/) based rootfs

The build script can be run using the `-h` parameter for more information about
arguments and environment variables.

These images can be signed using `sign_image.sh` in order to do verified updates.

## Quick getting started guide
To quickly integrate these tools
```console
git init <my_build_project>
cd <my_build_project>
git submodule add <esbs build URL> esbs
ln -f -s "esbs/docker_build.sh" "docker_build.sh"
./docker_build.sh -h
./docker_build.sh -b "My board config"
```

For further examples, see the [test](test/fixtures/configs) directory of this
repository.


## Testing
The [test](test) directory contains a tests for various scenario's. While these
are currently very bare bones, they can be run with:
```console
./docker_run_tests.sh`
```


## Requirements
To be able to generate an image, the following external artifacts are required.

* An already built kernel
* A device tree blob


### Template requirements
Included is a template that can be used for most common tasks. This includes
supporting multiple FDT's in images and configurations.

As the template makes certain assumptions/has certain requirements the
following is needed for the template:

* U-Boot that supports the selected hashing algorithm
* U-Boot that supports the used crypto cipher (only rsa variants for now)
* U-Boot that supports configuration node signature checking


## Using this repository
The make use of the build system, create a new repository, include
this repository as a submodule and a directory holding configuration files
called `configs`. So for example:
```
build-project
├── configs
└── esbs
```


## Configuration
The `configs` directory contains build configuration files. If no configuration
directory is supplied to the build script via the `-b` parameter, the first
entry will be used.

If no config directory exists yet, supplying the `-b` parameter will try to
create one.
```console
./docker_build.sh -b configs/<board>
```
Note, that by default the configs directory is searched for configurations.
However when creating a new configuration, it must be supplied, as any directory
can be used.

As the directory is used as configuration name, a configuration directory, empty
or not must always be supplied.


### Configuration types
Several configuration types are supported, this section covers the what and how.


#### FIT image source (its)
A fit image file with the extension `.its` describing how to assemble a fit
fit image. The file name itself will be used to determine the output file name.
The build script will do a simple find/replace with the following markers.

* `@ARCH@` - target architecture, (e.g. `armv7`)
* `@BOARD_NAME@` - The name of the board (final part of the `-b` parameter)
* `@BUILD_DIR@` - Location of the generated files (e.g. `.build/${arch}/${build_name}`)
* `@BUILD_ID@` - Unique build identifier (based on timestamp)
* `@COMPRESSION_TYPE@` - Compression used for files (e.g. `gzip`)
* `@DATA_DEVICETREE@` - The final location of the devicetree blob
* `@DATA_INITRAMFS@` - The final location of the init ramdisk
* `@DATA_KERNEL@` - The final location of the kernel
* `@VERSION@` - Version of the final output (e.g. v0.1.2-rc1-dirty)

The filename may also end in `.its.in` for version control naming purposes.

Note that as the parser search for files ending in `.its*`, filenames that
contain this pattern could be mistaken.


#### CPIO archive (cpio.apklist)
To generate an initramfs to be used in a FIT image, a list of packages can be
supplied to the build script or supplied via a `.cpio.apklist` file in the
`configs` sub-directory, where one package is listed per line to be put into the
initramfs.

The file `initramfs.cpio.apklist` can be used to replace the initramfs supplied
via the -i parameter. Otherwise, the output is an initramfs cpio file.

Any image filename can be used to generate an cpio archive as output, but the
`initramfs` one is treated specially as an initramfs.


#### Repositories (repository files)
To generate an image, repositories are needed to be defined. By default
the standard Alpine CDN is used for Alpine images, but any repository can be
used via `-r` parameter or via a `.repositories` file in the configuration
subdirectory. The repository used needs to match the archive to build.

So for example, an Initramfs file named `initramfs` can have both the `.apklist`
and additionally a `.repository` file.


#### SquashFS images (squashfs.apklist)
A squashfs based image can be created as well. This is unlikely to be used in a
fitImage however. To build a rootfs image file, a list of packages is to be
supplied to the build script of via a `.squashfs.apklist` file in the `configs`
sub-directory, where one package is listed per line to be put into the squashfs
based root filesystem image.


#### Additional image files (.files)
To add additional files to an image, a directory containing these files can
be added which will be copied verbatim (`cp -a`). This can be useful to add
development files, that are not yet packaged. To do so a directory with
the extension `.cpio.files` or `.squashfs.files` are needed in conjunction with
the respective `.cpio.apklist` or `.squashfs.files` as described in the previous
section.


#### Rootfs image
To build a final rootfs image, potentially relying or containing any of the
above, the `rootfs` file prefix can be used. As the same extensions mentioned
earlier can be used, any supported image can be built. The final name will be
stripped off all pre- and post-fixes and will end in `.img`. Unless the file
name consists of only the `rootfs` prefix, in that case, the final image will
be `rootfs.img`. So for example, the file `rootfs.myimage.squashfs.apklist`
will generate a squashfs image, containing alpine based packages and will be
named `myimage.img`. Likewise a file named `rootfs.otherimage.cpio.apklist` will
be a cpio archive, containing an alpine based packages and be called
`otherimage.img`.

As many images as desired can be configured to be built, as long as there are
no conflicts in naming. E.g. `rootfs.cpio.apklist` and `rootfs.squashfs.apklist`
are conflicting and the last one would overwrite any previous ones.


#### fitImage
To build a fitImage with an initramfs, only a file starting with `fitImage*` is
needed. For example, to build a fitImage using the kernel and devicetree from
the within the initramfs, a file called `fitImage.setup.cpio.apklist` would
generate an initramfs with the files listed within, extract the kernel from it
and build a fitImage called `fitImage.setup.cpio.itb`. If no kernel is found
within, the kernel etc will still need to be supplied via parameters.


##### Template rootfs fitImage
When building a rootfs image, it is often desired to include a fitImage. The
fitImage expands the `-f` and `-k` parameters and gives them extra
functionality.


###### fitImage kernel
For the kernel, this means that a kernel is pulled from the rootfs, and moved
into the fitImage. When nothing is supplied via the `-k` parameter, the kernel
is expected to linked via a symlink as `<rootfs>/boot/vmlinu?`. If `-k`
contains only a name, this will be used to match a filename within the rootfs.
E.g. `-k kernel-v1.2.3` would use`<rootfs>/boot/kernel-v1.23` as its kernel.

Likewise, device tree's can be generated and added to the fitImage. The `-f`
parameter can be used to supply several different forms.

A file in itself, which will take the directory of the supplied file, add all
dtb's from the the top level there and use the linked dtb as the 'main'
configuration entry.
If only a name is supplied (recommended) the `<rootfs>/boot/dtbs/` directory
will be scanned for dtb's and added, where the name is used for the default
entry. If it contains a directory, all dtb's from that directory are added
and the first entry  will be used to set the default. If the argument is not
supplied, all dtbs from the top level `<rootfs>/boot/dtbs` are added, and the
first entry is set as default.
To supply both a directory and a name, the following form should be used
`<path_to_dir>/<name>` which looks as if a file or directory where supplied
but neither are. The script will check the path and use that for the list of
dtbs and takes the name to use as default. Absolute paths must be used if a
directory is indended as all relative paths are relative to `<rootfs>/boot/dtbs`
and so `<path>/<name>` leads to `<rootfs>/boot/dtbs/<path>`.

It is important to realize, all configurations are still available when using
`-f <name>`, this only impacts the location. U-Boot can load any configuration
that is needed.

Note, that a relative path is checked for existence, so do not use a
`<path>/<name>` that also resolves. This can become especially confusing within
docker contexts. The script tries its best to do the right thing.


###### fitImage initramfs
To offer a little bit more flexibility to the standard build script, it is
possible to generate additional initramfs images and match them to the fitImage.
The build script will match the file name which starts with `initramfs.`
followed by the name of a devicetree, and ending with `.cpio.*list` or
`.squashfs.*list` in the configuration section of the fitImage. Thus if the
fitImage were to have two devicetree's, `board1.dtb` and `board2.dtb` which
would result in a configuration section of `conf-board1` and `conf-board2` a
initramfs of `initramfs.board2.cpio.apklist` would generate an initramfs and
link it to `conf-board2`. If via some other way another initramfs was set for
it would be used for all other configurations, including `conf-board1`, unless
of course a file `initramfs.board1.cpio.apklist` were to exist.


#### fitImage FGPA bitstreams
To add an FPGA bitstream, any files ending with the extension `.bit` or `.bin`
that are found within the rootfs in `/lib/firmware/fpga/` or in the `configs/*`
directory will be added. Alternatively the `-f` parameter can be used to
explicitly set a bitstream or provide a name to limit the results in
`/lib/firmware/fpga/` for all configurations, when configurations are generated.

Additionally, if a (symlinked) bitstream file matches in the filename (basename)
to a devicetree, then those two are coupled in the fitImage configuration.

Thus if a devicetree `my_devicetree.dtb` exists, the matching
`/var/lib/fpga/**/my_devicetree.bit` will be treated as one single configuration
directive.


#### U-Boot scripts (.scr and .scr.its.in)
To generate a `.uimg`, a U-Boot script, for example to load over the network
when doing a network boot, a regular U-Boot script needs to be compiled into
fit script image. To do so, a file with the extension `.scr` is to be placed
in the configuration directory. If the default template is adequate, nothing
else is needed, however otherwise, a file with the extension `.scr.its.in` is
is needed to. The filename needs to be identical otherwise. In addition to the
earlier mentioned variables, a `.scr.its.in` file also translates the location
of the script itself to be inserted as such:

* `@DATA_SCRIPT@` - The final location of the script


## Building
The build script takes the required artifacts to generate a FIT image. To do so
run the build script with sufficient parameters.

It is most convenient to symlink this script locally
```console
ln -f -s "esbs/docker_build.sh"
```

Thus building then becomes for example:
```sh
./docker_build.sh -b "my_board_config" -f "example.dtb" -k "zImage" -i "zInitramfs"
```

For more information see `./docker_build.sh -h`


### Caches
The build cache is cleared by default. This to ensure reliable builds from
scratch. However as this is quickly very inconvenient, the `-c` flag to the
build script can be passed to keep the existing build cache in `.build`. Please
however be aware of the potential impact re-using the build cache can have.


### Local build
There is a simple script available, `buildenv_check.sh` to verify the basic
needs of the build, the build environment check currently only checks if
all needed programs are available. For other additional requirements check
the Dockerfile.


### Bootstrap alpine rootfs
To build an alpine based rootfs image the `bootstrap_alpine_rootfs.sh` script can
be used. This script will bootstrap alpine Linux using the supplied packages or
package list file.

Due to needs of the script, this must be done from an alpine based
container/system.

When not using docker to build/bootstrap, administrative privileges are required.


### Secure boot
Secure boot, verified boot, DRM boot or High Assurance boot, are all names for
the same thing, cryptographically verifying that the CPU only boots signed code.

This usually starts at the SoC/CPU, which has fuses where a (hash of the) public
key is stored and it is used to verify the payload against. This payload is
often a bootloader. The bootloader is part of the created image, and probably
contains or has access to a public key which it in turn uses to load the next
stage. If this next stage were to be a fitImage, the fitImage needs to be
signed with the secure boot key (pair). To do so, a private key or optionally
a key pair are to be supplied with the `-s` parameter.

The key can be delivered in 2 forms, either as a variable where both keys are
concatenated, and the script pulls them apart and stores them in two temporary
files (mktemp). Often useful for a CI, or as one (or two) separate file(s). If
both the private and public key are supplied, the `-s` parameter takes the name
of the private key, and expects the public key to exist with the `.pub` suffix.
If no public key is provided, a new one is generated instead, from the supplied
private key.

These keys are then passed onto `mkimage` for signing the fitImage with. This
however only works when there is a `configurations` node in the `.its` file
as only configuration nodes are signed. Signing only the images is not
supported as this is not deemed secure. A `mkimage` with these patches is
therefore required for this.

NOTICE: Because not all signing features are merged upstream, a suitable mkimage
needs to be supplied. This can be done for example via:
```sh
OPT_DOCKER_ARGS="-v <path_to>/mkimage:/usr/bin/mkimage" ./docker_build.sh
```

## Verified images
By leveraging Public Key Infrastructure (PKI), a trusted root certificate can be used
to verify an update file.

The initial requirement is: The target has a trusted root certificate in place.

From the root certificate, a certificate chain is needed that contains all intermediate certificates in between
the root and a leaf certificate. The private key associated to this leaf
certificate is used to sign an image file.

After concatenating all the certificates in the `chain` file and using the matching private key an image can be signed:
```sh
./docker-runner.sh sign_image.sh -c "chain" "image_file" -k "leaf_private.pem"
```

If no output file name is specified, the suffix ".sign" will be appended to original file name, once signed.

The verification process will need to extract this certificate chain, verify it against it's
local, e.g. the targets, root certificate, and verify the signature authenticity with the leaf certificate provided.

An implementation of this verification can be found in the tests of the [updater](https://gitlab.com/esbs/updater/).

### Verified image format
To an image (a payload) is appended:
* Chain of certificates including the leaf certificate
* 4 bytes in little-endian indicating the size of the combination of certificates
* An OpenSSL (digest) signature
* 4 bytes in little-endian indicating the size of the signature

```
+----------------+---------+
| Payload        | n-bytes |
| Zero marker    | 4 bytes |
+----------------+---------+
| Cert chain     | n-bytes |
| Chain size     | 4 bytes |
+----------------+---------+
| Signature      | m-bytes |
| Signature size | 4 bytes |
+----------------+---------+
```

This structure is chosen, so that it is possible to read data from the end of
the file, and parse it backwards, allowing for cheap manipulation. In the case
of a squashfs payload, nothing is needed, it can simply be mounted. Other
payloads may decide to truncate the file at the zero byte marker.
Further more inserting data allows for backwards compatibility. A newer firmware
update would try to decode the number of bytes before the signature and get
the zero marker instead, indicating that whatever payload it did expect is not
there.
