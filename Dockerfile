# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

FROM registry.hub.docker.com/library/alpine:edge

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

# Work around busybox issue #11601 by adding util-linux explicitly.
RUN \
    apk add --no-cache \
        bzip2 \
        dtc \
        git \
        gzip \
        lz4 \
        openssl \
        shunit2 \
        squashfs-tools \
        uboot-tools \
        util-linux \
        xxd \
        xz \
        zstd \
    && \
    rm -f -r "/var/cache/apk/"*

COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
